package com.example.simple.rest.app;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Duration;
import java.time.LocalDateTime;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(Model model) {
        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
        final long uptime = rb.getUptime();
        model.addAttribute("time", LocalDateTime.now());
        model.addAttribute("uptime", Duration.ofMillis(uptime));
        return "index";
    }
}
