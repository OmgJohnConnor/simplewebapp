package com.example.simple.rest.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.stream.LongStream;

@SpringBootTest
class SimpleRestAppApplicationTests {

    @Test
    void contextLoads() {
        int number = 1_000;
        long start = System.currentTimeMillis();
        BigDecimal result = LongStream.rangeClosed(2, number)
                .boxed()
                .parallel()
                .map(BigDecimal::valueOf)
                .reduce(BigDecimal.ONE, BigDecimal::multiply);
        System.err.println("Total: " + (System.currentTimeMillis() - start) + "ms");
    }
}
