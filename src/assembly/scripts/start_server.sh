#!/bin/bash
cd /home/ubuntu
sudo pkill "java"
#run in background
sudo nohup java -jar SimpleWebApp-0.0.1-SNAPSHOT.war --server.port=8080  > /dev/null 2> /dev/null < /dev/null &
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080